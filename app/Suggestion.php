<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

use Carbon\Carbon as Carbon;

class Suggestion extends Model
{
    protected $table = 'suggestions';

    protected $fillable = ['snack_name', 'snack_id', 'snack_location'];

    public function votes() {

        return $this->hasMany('App\Vote', 'suggestion_id');

    }

    public static function validate($data)
    {
        $rules = [
            'snack' => 'required_without_all:name,location',
            'name' => 'required_without:snack|required_with:location',
            'location' => 'required_without:snack|required_with:name'
        ];

        $messages = [
            'snack.required_without_all' => 'Please select a snack from the dropdown or enter the information below.',
        ];

        return Validator::make($data, $rules, $messages);
    }

    public static function getActive() {
        $firstDay = new Carbon('first day of this month');
        $firstDay->hour = 0;
        $firstDay->minute = 0;
        $firstDay->second = 0;
        $lastDay = new Carbon('first day of next month');
        $lastDay->hour = 0;
        $lastDay->minute = 0;
        $lastDay->second = 0;
        $suggestions = Suggestion::whereBetween('created_at', [$firstDay, $lastDay])->get();

        return $suggestions;
    }
}
