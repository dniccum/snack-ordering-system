<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'VotesController@showAllSnacks');
Route::get('/vote', 'VotesController@placeVote');
Route::get('/suggest', 'SuggestionsController@showSuggestions');
Route::post('/suggest', 'SuggestionsController@addSuggestion');
Route::get('/shopping-list', 'ShoppingListController@showShoppingList');
