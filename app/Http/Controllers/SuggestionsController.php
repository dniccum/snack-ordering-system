<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Cookie;
use GuzzleHttp\Client;
use Carbon\Carbon as Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\Minutes as Minutes;
use App\Library\Snacks as Snacks;

use App\Suggestion as Suggestion;

class SuggestionsController extends Controller
{

    public function showSuggestions() {
        $snacks = Snacks::getList();

        $optional_snacks = [
            '' => 'Select a snack'
        ];
        foreach ($snacks as $snack) {
            if ($snack->optional) {
                $optional_snacks[$snack->id] = $snack->name;
            }
        }

        return view('suggestions')->with('snacks',$optional_snacks);
    }

    public function addSuggestion(Request $request) {
        $formData = $request->all();
        $validator = Suggestion::validate($formData);

        if ($validator->passes()) {
            if (app('request')->cookie('suggestion') == '1') {
                return back()->withErrors('You have already made your suggestion for this month. Sorry!')->withInput();
            } else if (count(Suggestion::where('snack_id', $request->snack)->first()) > 0) {
                return back()->withErrors('This suggestion has already been made!')->withInput();
            } else {
                if ($request->snack) {
                    // snack already added
                    $snacks = Snacks::getList();
                    foreach ($snacks as $snack) {
                        if (strval($snack->id) == strval($request->snack)) {
                            // adds to the database
                            $suggestion = new Suggestion;
                            $suggestion->snack_id = $snack->id;
                            $suggestion->snack_name = $snack->name;
                            $suggestion->snack_location = $snack->purchaseLocations;
                            if ($snack->lastPurchaseDate) {
                                $suggestion->last_purchased = $snack->lastPurchaseDate;
                            } else {
                                $suggestion->last_purchased = 'Never';
                            }
                            $suggestion->save();
                        }
                    }
                } else {
                    // adds to Api
                    $client = new \GuzzleHttp\Client();
                    $response = $client->post('https://api-snacks.nerderylabs.com/v1/snacks/?ApiKey=e08ec9ec-0f9a-4d72-8c51-194deefcbf38', [
                        'json' => [
                            'name' => $request->name,
                            'location' => $request->location
                        ]
                    ]);
                    if ($response->getStatusCode() == 200) {
                        $newSnack = json_decode($response->getBody()->getContents());

                        // adds to the database
                        $suggestion = new Suggestion;
                        $suggestion->snack_id = $newSnack->id;
                        $suggestion->snack_name = $newSnack->name;
                        $suggestion->snack_location = $newSnack->purchaseLocations;
                        $suggestion->last_purchased = $newSnack->lastPurchaseDate;
                        if ($newSnack->lastPurchaseDate) {
                            $suggestion->last_purchased = $newSnack->lastPurchaseDate;
                        } else {
                            $suggestion->last_purchased = 'Never';
                        }
                        $suggestion->save();

                    } else {
                        return back()->withErrors('An error has occured. Please try again.')->withInput();
                    }
                }
                return redirect('/')->withSuccess('Your new suggestion has been added.')->withCookie(cookie('suggestion', '1', Minutes::getTimeLeft()));
            }
        }

        return back()->withErrors($validator)->withInput();
    }

}
