<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Library\Snacks as Snacks;

use App\Suggestion as Suggestion;

class ShoppingListController extends Controller
{

    public function showShoppingList() {
        $snacks = Snacks::getList();
        $required_snacks = [];

        foreach ($snacks as $snack) {
            if (!$snack->optional) {
                array_push($required_snacks, $snack);
            }
        }
        $required_amount = 10 - count($required_snacks);

        $vote_leaders = Suggestion::with('votes')->orderBy('snack_name','desc')->limit($required_amount)->get()->sortByDesc(function($suggestions) {
            return $suggestions->votes->count();
        });

        $data = [
            'required_snacks' => $required_snacks,
            'vote_leaders' => $vote_leaders
        ];

        return view('shopping-list', $data);

    }

}
