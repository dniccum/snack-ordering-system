<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Client;
use Carbon\Carbon as Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Vote as Vote;
use App\Suggestion as Suggestion;

use App\Library\Snacks as Snacks;
use App\Library\Minutes as Minutes;

class VotesController extends Controller
{

    public function showAllSnacks() {
        $snacks = Snacks::getList();
        $suggestions = Suggestion::getActive();
        $votes = intval(app('request')->cookie('votes'));

        $required_snacks = [];
        foreach ($snacks as $snack) {
            if (!$snack->optional) {
                array_push($required_snacks,$snack);
            }
        }

        $data = [
            'required_snacks' => $required_snacks,
            'suggested_snacks' => $suggestions,
            'votes' => $votes
        ];
        return view('votes', $data);

    }

    public function placeVote(Request $request) {
        $votes = intval(app('request')->cookie('votes'));
        if (!$votes) {
            $votes = 0;
        }
        if ($votes < 3) {
            if ($request->ajax()) {
                $id = $request->suggestion_id;

                $vote = new Vote;
                $vote->suggestion_id = $id;
                $vote->save();

                return response()->json(['success' => true, 'message' => 'Thank you for your vote!'])->withCookie(cookie('votes', ++$votes, Minutes::getTimeLeft()));;
            } else {
                echo 'Invalid request.';
            }
        } else {
            return response()->json(['success' => false, 'message' => 'You are out of votes! See you next month.']);
        }

    }

}
