<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $table = 'votes';

    protected $fillable = ['name', 'snack_id'];

    public function suggestion() {

        return $this->belongsTo('App\Suggestion');

    }
}
