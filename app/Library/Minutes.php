<?php

namespace App\Library;

use Carbon\Carbon as Carbon;

class Minutes {

    public static function getTimeLeft() {
        $currentTime = strtotime(Carbon::now());
        $endTime = new Carbon('first day of next month');
        $endTime->hour = 0;
        $endTime->minute = 0;
        $endTime->second = 0;
        $endTime = strtotime($endTime);
        $minutes = round(abs($endTime - $currentTime) / 60);

        return $minutes;
    }
}
