<?php

namespace App\Library;

use GuzzleHttp\Client;
use Carbon\Carbon as Carbon;

class Snacks {

    public static function getList() {
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://api-snacks.nerderylabs.com/v1/snacks?ApiKey=e08ec9ec-0f9a-4d72-8c51-194deefcbf38');
        $snacks = json_decode($response->getBody()->getContents());

        return $snacks;
    }
}
