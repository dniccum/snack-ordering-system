var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 // global paths
var paths = {
    'jquery': './vendor/bower_components/jquery/',
    'materialize': './vendor/bower_components/materialize/',
    'ionicons': './vendor/bower_components/ionicons/'
};

//disable maps
elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix.sass('app.scss', 'public/css/', {
        includePaths: [
            paths.materialize + 'sass/',
            paths.ionicons + 'scss/'
        ]
    })
    .copy(paths.materialize + 'font/**', 'public/font')
    .copy(paths.ionicons + 'fonts/**', 'public/font')
    .scripts([
        paths.jquery + 'dist/jquery.js',
        paths.materialize + 'dist/js/materialize.js'
    ], 'public/build/js/vendor.js', './')
    .scripts([
        'main.js',
        'votes.js'
    ], 'public/build/js/app.js');
});
