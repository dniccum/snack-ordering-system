<?php

use Illuminate\Database\Seeder;
use App\Suggestion as Suggestion;

class SuggestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suggestions')->delete();

        $suggestions = [
            [
                'snack_name' => 'Chex Mix',
                'snack_location' => 'Walmart',
                'last_purchased' => null,
                'snack_id' => 1537
            ], [
                'snack_name' => 'Fiddle Faddle',
                'snack_location' => 'Target',
                'last_purchased' => null,
                'snack_id' => 1538
            ]
        ];

        foreach($suggestions as $suggestion){
            Suggestion::create($suggestion);
        }
    }
}
