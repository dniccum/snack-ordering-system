@extends('layouts.basic')

@section('title')Suggest A Snack @stop

@section('bodyClass')votes @stop

@section('pageContent')
    <div class="row" style="padding-top: 30px;">
        <div class="container">
            <div class="col l12 m12 s12">
                <div class="card white">
                    <div class="card-content">
                        <h4>Make a suggestion</h4>
                        {!! Form::open(['action' => 'SuggestionsController@addSuggestion']) !!}
                            @if (count($snacks) >= 1)
                                <p>Select a suggestion from the dropdown below.</p>
                                {!! Form::label('snack', 'Select a previously added snack.') !!}
                                {!! Form::select('snack', $snacks, Input::old('name'), ['class' => 'browser-default', 'style' => 'margin-bottom: 20px;']) !!}
                            @endif
                            <p>Don't see what you are looking for? Fill out the form below to add a new suggestion.</p>
                            <div class="input-field" style="margin-top: 20px">
                                {!! Form::text('name', Input::old('name'), ['class' => 'validate']) !!}
                                {!! Form::label('name') !!}
                            </div>
                            <div class="input-field">
                                {!! Form::text('location', Input::old('location'), ['class' => 'validate']) !!}
                                {!! Form::label('location') !!}
                            </div>
                            <div class="input-field">
                                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                                </button>
                            </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
