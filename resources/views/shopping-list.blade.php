@extends('layouts.basic')

@section('title')Shopping List For {{ Carbon::now()->format('F') }} @stop

@section('pageContent')
    <div class="row" style="padding-top: 30px;">
        <div class="container">
            <div class="col l12 m12 s12">
                <div class="card white">
                    <div class="card-content">
                        <h4>{{ Carbon::now()->format('F') }} Snack Shopping List</h4>
                        <div class="row">
                            <div class="col l12 m12 s12">
                                <table>
                                    <thead>
                                        <tr>
                                            <th data-field="name">Name</th>
                                            <th>Location can be found</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($required_snacks as $snack)
                                            <tr>
                                                <td>{{ $snack->name }}</td>
                                                <td>{{ $snack->purchaseLocations }}</td>
                                            </tr>
                                        @endforeach
                                        @foreach ($vote_leaders as $snack)
                                            <tr>
                                                <td>{{ $snack->snack_name }}</td>
                                                <td>{{ $snack->snack_location }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
