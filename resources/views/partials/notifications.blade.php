<script type="text/javascript">
    $(document).ready(function() {
        @if($errors->has())
            @foreach ($errors->all() as $error)
                Materialize.toast('{{ $error }}', 3000, 'red');
            @endforeach
        @endif
        @if ($message = Session::get('success'))
            Materialize.toast('{{ $message }}', 3000, 'green');
        @elseif ($message = Session::get('warning'))
            Materialize.toast('{{ $message }}', 3000, 'red');
        @endif
    });
</script>
