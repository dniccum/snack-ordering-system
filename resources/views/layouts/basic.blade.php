<!doctype html>
<html class="no-js" lang="en-us">
<head>
    <!-- META DATA -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!--[if IE]><meta http-equiv="cleartype" content="on" /><![endif]-->
    <!-- SEO -->
    <title>@yield('title') | SnaFoo - Snack Food Ordering System</title>
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-touch-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('images/favicon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="160x160" href="{{ asset('images/favicon-160x160.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
    <meta name="msapplication-TileImage" content="{{ asset('images/mstile-144x144.png') }}">
    <meta name="msapplication-TileColor" content="#ff0000">

    <!-- STYLESHEETS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body class="@yield('bodyClass') purple darken-4">
    <nav class="deep-purple darken-1" id="navBar">
        <div class="nav-wrapper">

            {{-- MOBILE NAVIGATION --}}
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            <ul id="nav-mobile" class="side-nav">
                <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/') }}">Vote</a></li>
                <li class="{{ Request::is('/suggest') ? 'active' : '' }}"><a href="{{ url('/suggest') }}">Suggest</a></li>
                <li class="{{ Request::is('/shopping-list') ? 'active' : '' }}"><a href="{{ url('/shopping-list') }}">Shopping List</a></li>
            </ul>
            {{-- *****************  --}}

            <div class="container">
                <a href="/" class="brand-logo">SnaFoo</a>
                <ul class="right hide-on-med-and-down">
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/') }}">Vote</a></li>
                    <li class="{{ Request::is('suggest') ? 'active' : '' }}"><a href="{{ url('/suggest') }}">Suggest</a></li>
                    <li class="{{ Request::is('shopping-list') ? 'active' : '' }}"><a href="{{ url('/shopping-list') }}">Shopping List</a></li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('pageContent')

    <script src="{{ asset('build/js/vendor.js') }}" type="text/javascript"></script>

    @yield('scripts')

    @include('partials.notifications')

    <script src="{{ asset('build/js/app.js') }}" type="text/javascript"></script>

</body>
</html>
