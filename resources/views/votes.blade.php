@extends('layouts.basic')

@section('title')Vote Your Snacks @stop

@section('bodyClass')votes @stop

@section('pageContent')
    <div class="row" style="padding-top: 30px;">
        <div class="container">
            <div class="col l4 m6 s12 offset-l8 offset-m6 right-align white-text">
                <h4>Votes Left: <span id="votes-remaining">{{ $votes ? 3 - $votes : 3 }}</span></h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="col l12 m12 s12">
                <div class="card white">
                    <div class="card-content">
                        <h4>Vote Your Snacks for {{ Carbon::now()->format('F') }}</h4>
                        <div class="row">
                            <div class="col l4 m4 s12">
                                <h5 class="purple-text text-darken-3 thin">Always Purchased</h5>
                                <table>
                                    <thead>
                                        <tr>
                                            <th data-field="name">Name</th>
                                            <th>Last Purch</th>
                                        </tr>
                                    </thead>
                                    <tbody id="required-listing">
                                        @foreach ($required_snacks as $snack)
                                            <tr>
                                                <td>{{ $snack->name }}</td>
                                                <td>{{ $snack->lastPurchaseDate ? $snack->lastPurchaseDate : 'Never' }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col l8 m8 s12">
                                <h5 class="purple-text text-darken-3 thin">Suggested</h5>
                                <table>
                                    <thead>
                                        <tr>
                                            <th data-field="name">Name</th>
                                            <th>Votes</th>
                                            <th>Last Purch</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="suggestion-listing">
                                        @if (count($suggested_snacks) > 0)
                                            @foreach ($suggested_snacks as $snack)
                                                <tr>
                                                    <td>{{ $snack->snack_name }}</td>
                                                    <td class="votes">{{ count($snack->votes) }}</td>
                                                    <td>{{ $snack->last_purchased ? $snack->last_purchased : 'Never' }}</td>
                                                    <td>
                                                        <button type="button" data-id="{{ $snack->id }}" class="waves-effect waves-light btn vote-button"><i class="ion-thumbsup left"></i> Vote</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="3" class="center-align bold purple-text">No results</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
