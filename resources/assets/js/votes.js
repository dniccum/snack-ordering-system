'use strict';

if ($('body').hasClass('votes')) {
    $('.vote-button').click(function() {
        var target = $(this).parent().parent().find('.votes');
        var currentNumber = Number(target.text());
        var id = $(this).data('id');
        currentNumber = ++currentNumber;

        $.ajax({
            url: '/vote?suggestion_id=' +  id,
            dataType: 'json',
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    // sets vote counter
                    var counter = Number($('#votes-remaining').text());
                    $('#votes-remaining').text(counter - 1);

                    target.text(currentNumber);
                    Materialize.toast(data.message, 3000, 'green');
                } else {
                    Materialize.toast(data.message, 3000, 'red');
                }
            },
            error: function(error) {
                console.log(error);
                Materialize.toast('There was a problem with your vote.', 3000, 'red');
            }
        });
    });
}
