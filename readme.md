## SnaFoo - A Snack Ordering System

This is a basic snack ordering system that allows users to suggest and vote for snacks for the office.

### Summary

1. Laravel - This application is built on the [Laravel PHP Framework](http://laravel.com/).
2. Bower - This application relies upon the [Bower](http://bower.io) framework to install front-end dependencies
3. Gulp - This application depends on [Gulp](http://gulpjs.com) and Elixer to build and compiile all front-end dependencies.
4. LAMP - This application works best while in a LAMP stack environment with the Apache mod_rewrite module enabled.

### Installation

#### 1. Install all requirements

As outlined above, this application requires Bower and Gulp to install and manipulate the front-end dependencies that the application uses. Both of these applications can be installed with **NPM** like so:

```
npm install -g bower gulp
```

*Note: They may require to be run as an Administrator.*

Once complete, install the Bower packages with:

```
bower install
```

And finally we need to install the Gulp packages with NPM like so:

```
npm install
```

Depending on the internet connect, this could take a sec. Once complete, we need to compile all of the assets using Gulp and Elixer tasks that we just downloaded. To do so, we need to use this command:

```
gulp --production
```

The production attribute not only compiles the assets, but also minifies them as well.

Lastly, we need to install Laravel's dependencies with Composer with:

```
composer install
```

#### 2. Environment Variables

Laravel utilizes environment variables to drive the application's configuration. Some examples are stored in the `.env.example` file within the repository. In order for Laravel to use this file, use the following command:

```
scp .env.example .env && vim .env
```

Once open, you should see something like this:

```
APP_ENV=local
APP_DEBUG=true
APP_KEY=SomeRandomString

DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
```

The items that we need to focus on are the APP and DB prefixed variables. For right now ignore, the APP_KEY variable.

Change the `APP_ENV` variable to `staging`.

Change the `APP_DEBUG` variable to `false`.

As for the DB variables, add the respective MYSQL/PSQL settings.

Once finished close and save the file.

#### 3. Artisan

Laravel uses Artisan to run all of its application specific commands (migrations, jobs, etc). To see all of the available commands, enter the following into your shell:

```
php artisan
```

First we need to set hash key that will help the application fend off malicious users. To set this key (that is referred to above), enter:

```
php artisan key:generate
```

Once complete we need to set up our database tables and then seed them. To migrate the database, enter the following command:

```
php artisan migrate
```

and then finally seed the database with:

```
php artisan db:seed
```

#### 4. Directory Permissions

Most likely the server that this application is stored on will require certain directories to have write access to. Give write access to the following:

* storage/
* bootstrap/cache/

#### 5. Complete!

Assuming everything was done correctly, you should see the home page.
